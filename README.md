# Kubernetes Backup Cronjob
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |
| <a name="provider_local"></a> [local](#provider\_local) | n/a |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [kubernetes_config_map_v1.config](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map_v1) | resource |
| [kubernetes_cron_job_v1.backup](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cron_job_v1) | resource |
| [kubernetes_secret_v1.extra](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret_v1) | resource |
| [kubernetes_secret_v1.key](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret_v1) | resource |
| [local_sensitive_file.password](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/sensitive_file) | resource |
| [random_integer.cronhour](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/integer) | resource |
| [random_integer.cronmin](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/integer) | resource |
| [random_password.password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cron"></a> [cron](#input\_cron) | Cron expression for when to perform backups. Leave as null for an autogenerated random time every day | `string` | `null` | no |
| <a name="input_extra_files"></a> [extra\_files](#input\_extra\_files) | Additional files to include in the container | <pre>list(object({<br>    name = string<br>    mount_path = string<br>    content = string<br>  }))</pre> | `[]` | no |
| <a name="input_name"></a> [name](#input\_name) | Backup job name, must be globally unique | `string` | n/a | yes |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Kubernetes namespace in which to create resources | `string` | n/a | yes |
| <a name="input_rsync_private_key"></a> [rsync\_private\_key](#input\_rsync\_private\_key) | Private key in OpenSSH format, with access to the backup user | `string` | n/a | yes |
| <a name="input_rsync_username"></a> [rsync\_username](#input\_rsync\_username) | Example ab1234s3 | `string` | n/a | yes |
| <a name="input_source_configuration"></a> [source\_configuration](#input\_source\_configuration) | Configuration for what to back up, as per docker-backupper spec | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->