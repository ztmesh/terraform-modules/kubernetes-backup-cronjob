resource "random_password" "password" {
  special = false
  length  = 64
  numeric = true
  lower   = true
  upper   = true
}

resource "local_sensitive_file" "password" {
  content  = random_password.password.result
  filename = "${path.root}/data/backup-passwords/${var.name}"
}

resource "kubernetes_config_map_v1" "config" {
  metadata {
    name      = "backupjob-${var.name}-config"
    namespace = var.namespace
  }

  data = {
    "backup.job" = <<EOT
[Job]
id=${var.name}
password=${random_password.password.result}

[Source]
${var.source_configuration}

[Destination]
driver=rsync.net
account=${var.rsync_username}
identity-file=/keys/key
folder=backups
EOT
  }
}

resource "kubernetes_secret_v1" "key" {
  metadata {
    name      = "backupjob-${var.name}-key"
    namespace = var.namespace
  }

  data = {
    "key" = var.rsync_private_key
  }
}

resource "kubernetes_secret_v1" "extra" {
  metadata {
    name      = "backupjob-${var.name}-extra"
    namespace = var.namespace
  }

  data = { for f in var.extra_files : f.name => f.content }
}

resource "random_integer" "cronhour" {
  min = 0
  max = 23
}

resource "random_integer" "cronmin" {
  min = 0
  max = 59
}

locals {
  crontime = var.cron != null ? var.cron : "${random_integer.cronmin.result} ${random_integer.cronhour.result} * * *"
}

resource "kubernetes_cron_job_v1" "backup" {
  metadata {
    name      = "backupjob-${var.name}"
    namespace = var.namespace
  }
  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 5
    schedule                      = local.crontime
    timezone                      = "Europe/London"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10
    job_template {
      metadata {}
      spec {
        backoff_limit              = 2
        ttl_seconds_after_finished = 10
        template {
          metadata {}
          spec {
            volume {
              name = "config"
              config_map {
                name = kubernetes_config_map_v1.config.metadata[0].name
              }
            }

            volume {
              name = "key"
              secret {
                secret_name = kubernetes_secret_v1.key.metadata[0].name
              }
            }

            volume {
              name = "extra"
              secret {
                secret_name = kubernetes_secret_v1.extra.metadata[0].name
              }
            }

            restart_policy = "Never"

            container {
              image             = "registry.gitlab.com/ztmesh/docker-backupper:main"
              name              = "backup"
              image_pull_policy = "Always"

              volume_mount {
                name       = "config"
                mount_path = "/etc/jobs"
              }

              volume_mount {
                name       = "key"
                mount_path = "/keys"
              }

              dynamic "volume_mount" {
                for_each = { for f in var.extra_files : f.name => f }

                content {
                  name       = "extra"
                  mount_path = volume_mount.value.mount_path
                  sub_path   = volume_mount.value.name
                }
              }
            }
          }
        }
      }
    }
  }
}
